ABOUT 
================================================================

Inspired by Amazon's [Managing PostgreSQL partitions with the pg_partman extension](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/PostgreSQL_Partitions.html).


As `pg-partman` doesn't come bundled by default in postgres, I attempted to create a Dockerfile for postgres15 with pg_partman in-built.

Reference: [pg-partman](https://github.com/pgpartman/pg_partman#installation) github

> **_Disclaimer:_**  
Do note that this is something I did for fun, and is by no means production-ready, so please use at your own risk.

Notes 
================================================================

## on [pg-cron](https://github.com/citusdata/pg_cron) ##
pg_cron can only be installed in one database in a database instance (aka the `main` database).

For this case, the "main" database would be the one defined in the env variable `$POSTGRES_DB`. Once running, unlike the `pg_partman` extension , `pg_cron` is not enabled by default and requires running the following SQL command:

```sql
CREATE EXTENSION IF NOT EXISTS pg_cron ;
```



